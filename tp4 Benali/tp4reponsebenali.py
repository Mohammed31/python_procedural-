from moduletp4 import triSelection
from moduletp4 import getDistance
from moduletp4 import getPoiClosest
from moduletp4 import getPoiInRange
from moduletp4 import sort 
from moduletp4 import partition
from moduletp4 import quicksort
from moduletp4 import readDB
from moduletp4 import sort_par_categorie 
from moduletp4 import afficher_les_categiries
from moduletp4 import sor_categori
import sys
import os


#je changez de repertoire courant, la ou se trouve votre ficher de BD
#os.chdir("C:/Users/bonj/Desktop/python/tp44")
print("")
print(" Bonjour ".center(80,"*") )

print(" Reponse du tp2 par BENALI ".center(80,"*") )

print("")


###################################################################################################################
###################################################################################################################

print(" je charge la base de donne  ".center(80,"*") )
print("")

result=readDB("point_dinteret.txt")
listglob=result[0]   # la liste globale de tuples [categorie, x, y]
np = result[1]  #le nombre de case de la liste qui presente le nombre de ligne

####################################################################################
#listglob,np=readDB("point_dinteret.txt") #cest une solution aussi
####################################################################################
print("***********************************************".center(80,"*"))
print("")
print("> Start ...!!! <".center(80,"*"))
print("")
print("***********************************************".center(80,"*"))
print("")
print(" tapez le numero correspondant a l'option souhaité ".center(80,"*") )
print("")
print("==>  1- rechercher le point d'intérêt le plus proche\n")

print("==>  2- afficher le nombre de points d'intérêt par catégorie ")
print("        dans la base de données\n")

print("==>  3- afficher le nombre de points d'intérêt par catégorie") 
print("        dans un rayon de 25, 50, 100, 250, 500m\n")

print("==>  4- rechercher les 30 points d'intérêt les plus proches") 
print("        triés du plus proche au plus éloigné\n")

print("==>  5- rechercher tous les points d'intérêt dans un rayon ")
print("        de 25, 50, 100, 250, 500m, triés du plus procheau ")
print ("        plus éloigné\n")

print(" que souhaitez vous  ".center(70,"*") )
print("")
choix=int(input("saisir une option "))
print("vous avez saisie ",choix)

def menu(choix1):
  print("")
  if choix1 == 1:
    print("******** vous souhaitez chercher par categorie ou sans ************\n")
    choix_ou_non=str(input("********* si oui tapez oui si non appuyer sur entrer directement ***********\n "))
    if len(choix_ou_non)==0:
      Xa=int(input("=======>  entrer votre position x "))
      Ya=int(input("=======>  entrer votre position y "))
      point_pret=getPoiClosest(listglob,Xa,Ya)
      print("")  
      print(" =====>  le point le plus pret :", point_pret[0])

    else:# je passe par le choix de la categorie 
    #jaffiche les categories
      n_categorie_choisi,tab_de_tout_catego=afficher_les_categiries()

      print("  vous avez choisi -->",tab_de_tout_catego[n_categorie_choisi])
      Xa=int(input("== entrer votre position x"))
      Ya=int(input("== entrer votre position y"))
      point_pret=getPoiClosest(listglob,Xa,Ya)
      # je parcour par categorie dans la liste globale deja triee par distance  
      #recuperer la categori choisi           
                       
      liste_une_seul_catego=sor_categori(point_pret,tab_de_tout_catego[n_categorie_choisi])
      #jaffiche tout les station dans la categorie choisie 
      print(liste_une_seul_catego[0])
      
      # i = 0 # Notre indice pour la boucle while
      # while i < len(liste_une_seul_catego):
      #   print(liste_une_seul_catego[i])
      #   i += 1
    
####################################################################################
  elif choix1 == 2:

    occurence1=sort_par_categorie (listglob)
    i = 0 # Notre indice pour la boucle while
    while i < len(occurence1):
      print("le nombre de",occurence1[i])
      i += 1

###################################################################################  
  elif choix1 == 3:
    rayon=int(input("saisir un rayon: \n"))
    Xa=int(input("entrer votre position x: \n"))
    Ya=int(input("entrer votre position y: \n"))
    list_par_rayon=getPoiInRange(listglob,Xa,Ya, rayon)

    petit_list=sort_par_categorie (list_par_rayon)
    
    i = 0 # Notre indice pour la boucle while
    while i < len(petit_list)-1:
      print("le nombre de",petit_list[i])
      i += 1
####################################################################################         
  elif choix1 == 4:
    print("******** vous souhaitez chercher par categorie ou sans ************\n")
    choix_ou_non=str(input("********* si oui tapez <oui> si non appuyer sur entrer directement ***********\n "))
    if len(choix_ou_non)==0:
      Xa=int(input("=======>  entrer votre position x "))
      Ya=int(input("=======>  entrer votre position y "))
      trent_point_pret=getPoiClosest(listglob,Xa,Ya)
      i = 0 # Notre indice pour la boucle while
      while i < 30:
        print(trent_point_pret[i])
        i += 1
    else:
      n_categorie_choisi,tab_de_tout_catego=afficher_les_categiries()

      print("  vous avez choisi -->",tab_de_tout_catego[n_categorie_choisi])
      Xa=int(input("==> entrer votre position x "))
      Ya=int(input("==> entrer votre position y "))
      trent_point_pret=getPoiClosest(listglob,Xa,Ya)

      liste_une_seul_catego=sor_categori(trent_point_pret,tab_de_tout_catego[n_categorie_choisi])        
      #jaffiche les 30 stations dans la categorie choisie 
      i = 0 # Notre indice pour la boucle while
      while i < 30:
        print(liste_une_seul_catego[i])
        i += 1
    
######################################################################################      
  elif choix1 == 5:
    print("******** vous souhaitez chercher par categorie ou sans ************\n")
    choix_ou_non=str(input("********* si oui tapez <oui> si non appuyer sur entrer directement ***********\n "))
    if len(choix_ou_non)==0:
      
      rayon=int(input("saisir un rayon: \n"))
      Xa=int(input("entrer votre position x:\n "))
      Ya=int(input("entrer votre position y:\n "))
      print("")
      list_par_rayon=getPoiInRange(listglob,Xa,Ya, rayon)
      if len(list_par_rayon)==0 :
        print("il ya rien.... !!!!!!")
      i = 0 # Notre indice pour la boucle while
      while i < len(list_par_rayon):
        print(list_par_rayon[i])
        i += 1
    else:
      n_categorie_choisi,tab_de_tout_catego=afficher_les_categiries()

      print("  vous avez choisi -->",tab_de_tout_catego[n_categorie_choisi])
      rayon=int(input("saisir un rayon: \n"))
      Xa=int(input("== entrer votre position x "))
      Ya=int(input("== entrer votre position y "))
      print("")
      list_par_rayon=getPoiInRange(listglob,Xa,Ya, rayon)
      liste_une_seul_catego=sor_categori(list_par_rayon,tab_de_tout_catego[n_categorie_choisi])        
      #jaffiche les 30 stations dans la categorie choisie 
      if len(liste_une_seul_catego)==0 :
        print("il ya rien.... !!!!!!")
      i = 0 # Notre indice pour la boucle while
      while i < len(liste_une_seul_catego):
        print(liste_une_seul_catego[i])
        i += 1
  else:
    print("je vous invite a relire le message de choix\n ")
    choix1=int(input("saisir une option a nouveau : "))
    menu(choix1) 

  return
menu(choix)    





# vous pouvez comparer la rapidité du tri par Sélection avec le quick sort
#a la ligne 131 vous enlever le commentaire commenter du ficher moduletp4
#           132 il faut la commenter du ficher moduletp4

#si vous choisissez de prendre le tri par sélection vous avez le temps de boire un café (c'est long)  
    
#   ligne 57, 58, 59  trois option de tri du ficher moduletp4  
#  ligne   89  trois option de tri du ficher moduletp4

# pour la  complexité  tri par sélection   Θ(n2)
# pour la  complexité  quick sort  Θ(n)  le pire des cas c’est Θ(n2) 

# les algorithmes mis en œuvre 
# getDistance   (tri Sélection ou  quick sort  ) 
# getPoiClosest  (tri + parcourt )
# getPoiInRange ( parcourt  )
# sort                   ( tri )
# readDB  (parcourt )
# sort_par_categorie     (tri Sélection ou  quick sort + parcourt  ) 

# afficher_les_categiries   ( parcourt )
# sor_categori      (parcourt + comparaison )

# 1 seconde  pour toute la base de 1milion 
# 0,5 seconde en moyenne  

# 10 000 point/ km^2
#sur un plan de 100km^2 et 1milion de point_dinteret 
# je considere que la voiture fait 1m 
#par deduction de la dencite donc j'ai 10 point dinteret par metre 
# la voiture ne doit pas bouger elle est deja arrivé 

# triSelection    Θ(n2)
# getDistance                2 operations donc complexité 2n 
# getPoiClosest              6 operation + 1 tri   [6n+ O(nlog(n))]
# getPoiInRange              4 operations +1 tri(Θ(n2)) + 1boucle(n2)
# afficher_les_categiries    1 operations+ 1 parcourt

# quand la voiture sort de la tuile a la tuile voisine 
# si non sa depend de sa facon de se deplacer 

# un temps resonable pour moi doit etre moins dune minute 
# dapres les calculs precedent sur 100 000km2 sa prend 
# une seconde donc sa depend de la surfece et le nombre de comparaison

#dapres le sujet doit charger les 4 tuiles avec celle ou il se trouve 

 